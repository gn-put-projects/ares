from random import randint, sample
class Person:
  def __init__(self, weight=65, load=10, floor=0, dest=2, ID=0, startTime=0):
    self.weight = weight
    self.load = load
    self.floor = floor
    self.dest = dest
    self.ID = ID
    self.startTime = startTime
    self.waiting = 0 #flaga czy został obsłużony
    
  def debugInfo(self):
    print("Hello, I weight {:d}kg. My load weights {:d}kg and I am going from floor {:d} to {:d}"
          .format(self.weight, self.load, self.floor, self.dest))

  def enterElevator(self, elevator):
    elevator.enter(self)

  def exitElevator(self, elevator):
    elevator.exit(self)

  def updateWaitTime(self, elevator):
    self.waitTime = elevator.time - self.startTime
  
  def getWaitTime(self, elevator):
    return elevator.time - self.startTime
  
  def onElevatorArrival(self, elevator):
    floor = elevator.floor
    if (floor == self.dest):
      elevator.exit(self)
      return
    if (floor == self.floor):
      elevator.enter(self)
      self.floor = -1
      return
    self.updateWaitTime(elevator)
    
def generateRandomPeople(n, floorCount):
  out=[]
  for i in range(n):
    floors = sample(range(floorCount), 2)
    out.append(Person(
      0,#randint(40, 120),
      0,
      floors[0],
      floors[1],
      i,
      i*50))
  #for i in range(5):
    #print(out[i].weight, out[i].load, out[i].floor, out[i].dest)
  return out

