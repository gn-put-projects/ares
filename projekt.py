from elevator import Elevator
from person import Person, generateRandomPeople
import pandas as pd

import plotly.express as px

import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output

from math import sqrt

app = dash.Dash(__name__)

def get_max_voltage_from_max_acceleration(max_a):
  #print(U0)
  return (((w*r)*(max_a*(Mw+Ml+Mp+Mk/2.0)-G*(Mp-Mw-Ml)))/(k*I[-1]))+U0

def u0_value():
  return -G*(Mp-Mw-Ml)*w*r/(k*I[-1])

def a_value(napiecie):
  # return (2*F[-1] + (Mmax - Ml)*G) / (4*Mw + 2*Ml + Mmax + k)
  #return (G*(Mp-Mw-Ml)+(k*napiecie*napiecie/Res)/(w*r))/(Mw+Ml+Mp+Mk/2.0)
  return (G*(Mp-Mw-Ml)+(k*napiecie)/(w*r))/(Mw+Ml+Mp+Mk/2.0)

def h_value():
  return min(Hmax, max(Hmin, H[-1] + A[-1]*Tp + A[-1]*Tp*Tp/2))
  #return min(Hmax, max(Hmin, H[-1] + Vf[-1]*Tp + A[-1]*Tp*Tp/2))
 
# USTAWIANIE ZMIENNYCH
# Masy [kg]
Mmax = 3000.0 #maksymalne obciążenie dla windy
Ml = 10.0 #masa ludzi w windzie (netto)
Mw = 1500.0 #masa windy (tara)
Mp = 1500.0
Mk = 1000.0 #masa kołowrotka

#przyspieszenia [m/s^2]
A = [0.0,] #przyspieszenie układu
G = 10.0 #przyspieszenie ziemskie

#wysokość [m]
Hzad = 15.0 # wysokość oczekiwana do osiągnięcia
H = [0.0, ] # wysokość windy
Hmin = 0
Hmax = 100.0

# CONTROLLER
PROP = [0.0, ]
INTE = [0.0, ]
DERI = [0.0, ]
kp  = 2326 # A maximum stable value
kp /= 2
kp = 0.025
ki = 0
kd = 0
e = [Hzad-H[-1], ] #uchyb
upi = [kp*sum(e), ]
Ti = 10000.0

#czas [s]
T = [0.0,] #czas
Tp = 1 # czas kroku pomiaru
V = [0.0, ] # prędkość
Vf = [0.0, ] # prędkość fiz

F = [1.0, ] #siła aktualna
Fmax = 1.0 #siła maksymalna
Fmin = 1.0 #siła minimalna

r = 0.5 # promien krazka
w = 1 # predkosc katowa

# MOTOR
k = 0.8 # sprawność
U = [0.0, ] # napięcie podawane na silnik [V]

I = [17.9, ] # natężenie prądu podawanego na silnik [A]
Res = 5
df = 5
wykres_h = 4
wykres_va = 3
wykres_reg = 2
ile_razy = 100000

#print("masy: ", (Mp-Mw-Ml))
#print("licznik: ", -G*(Mp-Mw-Ml)*w*r)
#print("mianownik: ", (k*I[-1]))
U0 = -G*(Mp-Mw-Ml)*w*r/(k*I[-1])
#print("u0v:",u0_value())
#print("u0:",U0)
Umin = -get_max_voltage_from_max_acceleration(5)
Umax =  get_max_voltage_from_max_acceleration(5)
Umin = 0.0
Umax = 4200.0

# KONIEC USTAWIANIA ZMIENNYCH GLOBALNYCH

def reset_variables():
  global w,r,Mw,Ml,Mp,Mk,A,G,Hzad,H,Hmin,Hmax,PROP,INTE,DERI, U0
  global kp,ki,kd,e,upi,Ti,T,Tp,V,Vf,F,Fmax,Fmin,k,U,Umin,Umax,I
  global df, wykres_h, wykres_va, wykres_reg, ile_razy
  # USTAWIANIE ZMIENNYCH
      
  #przyspieszenia [m/s^2]
  A = [0.0,] #przyspieszenie układu

  #wysokość [m]
  H = [0.0, ] # wysokość windy
  Hmin = 0
  Hmax = 100.0
  
  # CONTROLLER
  PROP = [0.0, ]
  INTE = [0.0, ]
  DERI = [0.0, ]
  kp  = 2326 # A maximum stable value
  kp /= 2
  kp = 0.025
  ki = 0
  kd = 0
  e = [Hzad-H[-1], ] #uchyb
  upi = [kp*sum(e), ]
  Ti = 10000.0
  
  #czas [s]
  T = [0.0,] #czas
  Tp = 0.1 # czas kroku pomiaru
  V = [0.0, ] # prędkość
  Vf = [0.0, ] # prędkość fiz
  
  F = [1.0, ] #siła aktualna
  Fmax = 1.0 #siła maksymalna
  Fmin = 1.0 #siła minimalna
  
  r = 0.5 # promien krazka
  w = 1 # predkosc katowa
  
  # MOTOR
  k = 0.8 # sprawność
  U = [0.0, ] # napięcie podawane na silnik [V]

  Res = [5, ]
  I = [17.9, ] # natężenie prądu podawanego na silnik [A]
  df = 5
  wykres_h = 4
  wykres_va = 3
  wykres_reg = 2
  ile_razy = 100000
  
  #print("masy: ", (Mp-Mw-Ml))
  #print("licznik: ", -G*(Mp-Mw-Ml)*w*r)
  #print("mianownik: ", (k*I[-1]))
  U0 = -G*(Mp-Mw-Ml)*w*r/(k*I[-1])
  #print("u0v:",u0_value())
  #print("u0:",U0)
  Umin = -get_max_voltage_from_max_acceleration(5)
  Umax =  get_max_voltage_from_max_acceleration(5)
  Umin = 0.0
  Umax = 4200.0
  
  # KONIEC USTAWIANIA ZMIENNYCH GLOBALNYCH

def simulate():
  global w,r,Mw,Ml,Mp,Mk,A,G,Hzad,H,Hmin,Hmax,PROP,INTE,DERI, U0
  global kp,ki,kd,e,upi,Ti,T,Tp,V,Vf,F,Fmax,Fmin,k,U,Umin,Umax,I
  global df, wykres_h, wykres_va, wykres_reg, ile_razy

  #U0 = u0_value()
  reset_variables()
  sigma = 0.0
  Ti = 100000
  ti = 1000

  kp = 227.25
  ki = 2.17
  kd = 1.05

  ile_razy2 = int(ile_razy*Tp)
  
  for N in range(1, ile_razy2):
    T.append(T[-1] + Tp)
    e.append(Hzad - H[-1])

    sigma += e[N]

    # V.append(V[-1] + A[-1]*Tp)
    
    # V.append((H[-1]-H[-2])/Tp)
    V.append(-1)
    Vf.append(Vf[-1] + A[-1]*Tp)

    #PROP.append(kp * (sigma*Tp/ti+e[-1]-e[-2]))                  # proportional
    PROP.append(kp * e[-1])                 # proportional
    INTE.append(INTE[-1] + ki * e[N] * Tp)  # integral
    DERI.append(kd * (e[-1] - e[-2])/Tp)    # derivative
    
    #upi.append(kp * (e[-1] + (Tp/Ti)*sigma))
    upi.append(PROP[-1] + INTE[-1] + DERI[-1])
    #upi.append(PROP[-1])
    U.append(min(Umax, max(Umin, U0 + upi[N])))
    #U.append(upi[-1])
    
    A.append(a_value((U[N])))
    
    H.append(h_value())

  df = pd.DataFrame({
    "Czas": T,
    "Wysokość windy": H,
    "Uchyb": e,
    "Napięcie": U,
    #"Średnia Prędkość": V,
    "Prędkość fiz": Vf,
    "Przyspieszenie": A,
    "Regulator - składowa P": PROP,
    "Regulator - składowa I": INTE,
    "Regulator - składowa D": DERI
  });
  
  wykres_h = px.line(df, x="Czas",
                   y=["Wysokość windy", "Przyspieszenie"])#, "Napięcie"])

  #wykres_va = px.line(df, x="Czas", y="Napięcie")

  wykres_reg = px.line(df, x="Czas", y=["Regulator - składowa P", "Regulator - składowa I", "Regulator - składowa D"])

  return wykres_h


  
people = generateRandomPeople(5, 10)

elevator = Elevator()
elevator.setpeople(people)
while(sum(elevator.inElevToFloor)>0 or len(elevator.people)):
  elevator.moveElevator()
  #elevator.printprop()
  elevator.updateTime(1)

def create_input_div(title, unit, ID, value, minv, maxv, vstep):
  input_box_style = {'flex': '1 1 auto', 'display': 'flex', 'align-items': 'center', 'flex-direction': 'column'}
  range_input_style = {'display': 'flex', 'align-items': 'center', 'flex-direction': 'row'}
  labelId = "{}-label".format(ID)
  inputId = "{}-input".format(ID)
  return html.Div([
    html.P("{}: {}{}".format(title, value, unit), id=labelId),
    html.Div([
      html.Span("{}{}".format(minv, unit)),
      dcc.Input(id=inputId, value = value, type="range",
                step=vstep, min=minv, max=maxv),
      html.Span("{}{}".format(maxv, unit))], style = range_input_style),
  ], style=input_box_style)

def serve_layout():
  input_box_style = {'flex': '1 1 auto', 'display': 'flex', 'align-items': 'center', 'flex-direction': 'column'}
  range_input_style = {'display': 'flex', 'align-items': 'center', 'flex-direction': 'row'}
  return html.Div([
  html.H1("ARES - Automatically Regulated Elevator Simulation", style={'text-align': 'center'}),
  html.Div([
    create_input_div("Masa ładunku windy", "kg", "ml", Ml, 0, 500, 10),
    create_input_div("Zadana wysokość", "m", "hzad", Hzad, 15, 30, 0.5)
    #create_input_div("Czas symulacji", "s", "ile_razy", ile_razy, 50, 500, 10)
  ], style={'width': '100%', 'display': 'flex'}),
    dcc.Graph(id='graph_h', figure=wykres_h),
    #dcc.Graph(id='graph_va', figure=wykres_va),
    dcc.Graph(id='graph_reg', figure=wykres_reg)
  ])

app.layout = serve_layout

@app.callback(Output('graph_h', 'figure'),
              #Output('graph_va', 'figure'),
              Output('graph_reg', 'figure'),
              Output('ml-label', 'children'),
              Output('hzad-label', 'children'),
              #Output('ile_razy-label', 'children'),
              Input(component_id = 'ml-input', component_property = 'value'),
              Input(component_id = 'hzad-input', component_property = 'value')
              #Input(component_id = 'ile_razy-input', component_property = 'value')
)
def update(ml, hzad):
  global w,r,Mw,Ml,Mp,Mk,A,G,Hzad,H,Hmin,Hmax,PROP,INTE,DERI, U0
  global kp,ki,kd,e,upi,Ti,T,Tp,V,Vf,F,Fmax,Fmin,k,U,Umin,Umax,I
  global df, wykres_h, wykres_va, wykres_reg, ile_razy
  if (hzad == None):
    print("hzad == None")
    return

  print("=== UPDATE REQUESTED ===")
    
  #if(time != None and height != None and max_temp != None and heat_loss != None):
  print("Setting Ml to {}".format(ml))
  Ml = float(ml)
  print("Value of Ml is {}".format(Ml))
  Hzad = float(hzad)
    
  #graph = px.line(df, x = "Time",
  #y = [df[element] for element in input_value], 
  #               labels = {'Time': 'Time elapsed since the beginning(h)', 'value': 'Value')
  simulate()

  return [
    wykres_h,
    #wykres_va,
    wykres_reg,
    "Masa ładunku windy: {}kg".format(Ml),
    "Zadana wysokość: {}m".format(Hzad)
  ]
  
if __name__ == '__main__':
  simulate()
  app.run_server(debug=False)
