from person import Person, generateRandomPeople
class Elevator:
  def __init__(self,):
    self.floor = 0
    self.goingTo = 0
    self.numFloors = 10
    self.people = []
    self.time =0
    self.floorHeight = 5
    self.weight = 0
    self.numFloors = 10
    self.weightLeaved = [0 for i in range(self.numFloors)] #array wag ludzi jadących na dane piętro, indeks pietro wartość waga
    self.requests = [0 for i in range(self.numFloors )] #array najdluższych oczekiwań dla danego piętra będąc na piętrze
    self.inElevToFloor = [0 for i in range(self.numFloors )] # array najdluższych oczekiwać na dane piętro będąc w windzie
    self.levels = []
    self.weights = []
    self.Mmax = 10000

  def printprop(self):
    self.showpeople()
    print("Floor")
    print(self.floor)
    #print("Going to")
    #print(self.goingTo)
    #print("NumFloors")
    #print(self.numFloors)
    #print("People")
    #print(self.people)
    print("Time")
    print(self.time)
    print("Weight")
    print(self.weight)
    print("WeightLeaved")
    print(self.weightLeaved)
    print("Requests")
    print(self.requests)
    print("inElevToFloor")
    print(self.inElevToFloor)
    print("Levels")
    print(self.levels)
    print("weights")
    print(self.weights)
  
  def setpeople(self,people):
    for i in range(len(people)):
      self.people.append(people[i])
  
  def showpeople(self):
    print("................................")
    for i in range(len(self.people)):
      print(self.people[i].weight, self.people[i].load, self.people[i].floor, self.people[i].dest,self.people[i].startTime,self.people[i].waiting)
  
  def setrequests(self):
    for man in self.people:
      if man.startTime < self.time:
        man.waiting = 1
        if self.requests[man.floor] < self.time - man.startTime:
           self.requests[man.floor] = self.time - man.startTime

  #to samo ale inna deklaracja petli i dziala
  def enter2(self):
    for man in self.people:
      if man.floor == self.floor:
        if self.weight + man.weight + man.load < self.Mmax :
          if man.waiting == 1:
            self.weight += man.weight + man.load
            self.requests[man.floor] = 0
            self.weightLeaved[man.dest] += man.weight + man.load 
            if self.inElevToFloor[man.dest] < man.getWaitTime(self):
              self.inElevToFloor[man.dest] = man.getWaitTime(self)
            indeks = self.people.index(man)
            self.people.pop(indeks)
    self.weights.append(self.weight)

  def exit(self):
    self.weight -= self.weightLeaved[self.floor]
    self.weightLeaved[self.floor] = 0
    self.inElevToFloor[self.floor] = 0
  
  #jedzie do najdłużej czekającej osoby i staje na każdym piętrze gdzie coś ma siąść/wysiąść
  def makeDec(self):
    maks = max(self.requests + self.inElevToFloor)
    levelToGo = (self.requests + self.inElevToFloor).index(maks)
    if levelToGo >= self.numFloors:
      levelToGo -= self.numFloors
    for i in range(self.floor,levelToGo):
      if self.requests[i] > 0 or self.inElevToFloor[i] > 0:
        levelToGo = i
        break
    self.goingTo = levelToGo
    self.floor = levelToGo
    return levelToGo
  
  def moveElevator(self):
    self.setrequests()
    self.levels.append(self.makeDec())
    self.exit()
    self.enter2()
    
  def updateTime(self,Time):
    self.time +=Time
    for i in range(self.numFloors):
      if self.inElevToFloor[i] > 0:
        self.inElevToFloor[i] += Time
